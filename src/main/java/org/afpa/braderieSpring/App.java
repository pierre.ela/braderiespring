package org.afpa.braderieSpring;

import org.afpa.braderieSpring.service.ServicePanier;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Hello world!
 *
 */

@ConfigurationProperties(prefix = "application.properties")
@SpringBootApplication
@Configuration
@PropertySource ("classpath:application.properties")
public class App 
{
//	@Bean
//	public DataSource getDatasource() {
//		return DataSourceBuilder.create()
//	             .driverClassName("org.postgresql.Driver")
//	             .url("jdbc:postgresql://93.16.53.240:5432/braderie")
//	             .username("lab")
//	             .password("lab123")
//	             .build();
//	}
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
      
        try(AnnotationConfigApplicationContext ctx= new AnnotationConfigApplicationContext(App.class)){
		System.out.println(ctx);
		ServicePanier servP=ctx.getBean(ServicePanier.class);
		
		System.out.println(servP.getByIdser(1));
		System.out.println(servP.getById(5).get());
		servP.updateQte(5, (servP.getById(5).get().getQuantite())+1);
		//servP.delete(servP.getById(5));
        }
        
    }
}
