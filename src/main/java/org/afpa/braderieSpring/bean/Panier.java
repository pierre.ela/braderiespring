package org.afpa.braderieSpring.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Pierre
 *
 */
@Entity
@Table(name="t_panier")
public class Panier {
	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	private Integer idpanier;
	private Integer iduser;
	private Integer idarticle;
	private Integer quantite;
	
	
	public Panier() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * @param iduser
	 * @param idarticle
	 * @param quantite
	 */
	public Panier(Integer iduser, Integer idarticle, Integer quantite) {
		super();
		this.iduser = iduser;
		this.idarticle = idarticle;
		this.quantite = quantite;
	}


	/**
	 * @return the iduser
	 */
	public Integer getIduser() {
		return iduser;
	}


	/**
	 * @param iduser the iduser to set
	 */
	public void setIduser(Integer iduser) {
		this.iduser = iduser;
	}


	/**
	 * @return the idarticle
	 */
	public Integer getIdarticle() {
		return idarticle;
	}


	/**
	 * @param idarticle the idarticle to set
	 */
	public void setIdarticle(Integer idarticle) {
		this.idarticle = idarticle;
	}


	/**
	 * @return the quantite
	 */
	public Integer getQuantite() {
		return quantite;
	}


	/**
	 * @param quantite the quantite to set
	 */
	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}


	/**
	 * @return the idpanier
	 */
	
	public Integer getIdpanier() {
		return idpanier;
	}

}
