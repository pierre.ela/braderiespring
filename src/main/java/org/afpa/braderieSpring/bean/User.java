/**
 * 
 */
package org.afpa.braderieSpring.bean;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author samia
 *
 */

	@Entity
	@Table(name="t_user")
	public class User {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Integer iduser;
		private String login;
		private String pass;
		private Integer nbconnexion;

		public User(Integer iduser, String login, String pass, Integer nbconnexion) {
			this.iduser = iduser;
			this.login = login;
			this.pass = pass;
			this.nbconnexion = nbconnexion;
		}

		public User() {

		}

		/**
		 * @return the iduser
		 */
		public Integer getIduser() {
			return iduser;
		}
		/**
		 * @param iduser the iduser to set
		 */
		public void setIduser(Integer iduser) {
			this.iduser = iduser;
		}
		/**
		 * @return the login
		 */
		public String getLogin() {
			return login;
		}
		/**
		 * @param login the login to set
		 */
		public void setLogin(String login) {
			this.login = login;
		}
		/**
		 * @return the pass
		 */
		public String getPass() {
			return pass;
		}
		/**
		 * @param pass the pass to set
		 */
		public void setPass(String pass) {
			this.pass = pass;
		}
		/**
		 * @return the nbConnexion
		 */
		public Integer getNbconnexion() {
			return nbconnexion;
		}
		/**
		 * @param nbConnexion the nbConnexion to set
		 */
		public void setNbconnexion(Integer nbconnexion) {
			this.nbconnexion = nbconnexion;
		}
	}
