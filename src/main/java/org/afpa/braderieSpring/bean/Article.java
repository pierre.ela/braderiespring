package org.afpa.braderieSpring.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_ARTICLE")
public class Article {

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idarticle", unique=true, nullable=false)
	private Integer idArticle;
	
	@Column(name="description")
	private String description;
	
	@Column(name="marque")
	private String marque;
	
	@Column(name="prixunitaire")
	private Integer prixUnitaire;
	
	private Integer quantite;
	
	/**
	 * 
	 */
	public Article() {
		super();
	}

	@Override
	public String toString() {
		return "Article [idArticle=" + idArticle + ", description=" + description + ", marque=" + marque
				+ ", prixUnitaire=" + prixUnitaire + ", quantite=" + quantite + "]";
	}


	/**
	 * @param idArticle
	 * @param description
	 * @param marque
	 * @param prixUnitaire
	 * @param quantite
	 */
	public Article(Integer idArticle, String description, String marque, Integer prixUnitaire, Integer quantite) {
		super();
		this.idArticle = idArticle;
		this.description = description;
		this.marque = marque;
		this.prixUnitaire = prixUnitaire;
		this.quantite = quantite;
	}

	/**
	 * @param idArticle
	 * @param description
	 * @param marque
	 * @param prixUnitaire
	 */
	public Article(Integer idArticle, String description, String marque, Integer prixUnitaire) {
		super();
		this.idArticle = idArticle;
		this.description = description;
		this.marque = marque;
		this.prixUnitaire = prixUnitaire;
	}


	/**
	 * @return the idArticle
	 */
	public Integer getIdArticle() {
		return idArticle;
	}


	/**
	 * @param idArticle the idArticle to set
	 */
	public void setIdArticle(Integer idArticle) {
		this.idArticle = idArticle;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the marque
	 */
	public String getMarque() {
		return marque;
	}


	/**
	 * @param marque the marque to set
	 */
	public void setMarque(String marque) {
		this.marque = marque;
	}


	/**
	 * @return the prixUnitaire
	 */
	public Integer getPrixUnitaire() {
		return prixUnitaire;
	}


	/**
	 * @param prixUnitaire the prixUnitaire to set
	 */
	public void setPrixUnitaire(Integer prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}


	/**
	 * @return the quantite
	 */
	public Integer getQuantite() {
		return quantite;
	}


	/**
	 * @param quantite the quantite to set
	 */
	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}
	
	
	
	
	
	
}
