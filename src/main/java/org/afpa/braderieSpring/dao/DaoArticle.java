package org.afpa.braderieSpring.dao;

import java.util.Map;

public interface DaoArticle<T> {

	public Map<Integer, T> findAll();

	public T findByid(T article);

	public T create(T article);

	public T update(Integer idarticle);

	public void delete(Integer idarticle);
}
