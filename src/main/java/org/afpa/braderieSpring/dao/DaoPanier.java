package org.afpa.braderieSpring.dao;

import org.afpa.braderieSpring.bean.Panier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Pierre
 *
 */
@Repository
public interface DaoPanier extends CrudRepository<Panier, Integer>{

		public Iterable<Panier> getByIduser (int idUser);
		public Panier getByIdpanier(int idpanier);

}
