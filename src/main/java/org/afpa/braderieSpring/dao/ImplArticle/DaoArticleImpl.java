package org.afpa.braderieSpring.dao.ImplArticle;

import java.util.HashMap;
import java.util.Map;

import org.afpa.braderieSpring.bean.Article;
import org.afpa.braderieSpring.dao.DaoArticle;
import org.springframework.stereotype.Repository;


@Repository
public class DaoArticleImpl implements DaoArticle<Article>{
	
	MockArticle ma = new MockArticle();
	Map<Integer, Article> listArticle = ma.getMockArticle();

	@Override
	public HashMap<Integer, Article> findAll() {
		System.out.println("Dans DaoArticleImpl.findAll()");
		ma.getMockArticle().entrySet()
		.stream()
		.forEach(article -> System.out.println(article.getValue().getIdArticle()+" "+article.getValue().getDescription()));
		return (HashMap<Integer, Article>) listArticle;
	}

	@Override
	public Article findByid(Article article) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Article create(Article article) {
		listArticle = ma.getMockArticle();
		listArticle.put(article.getIdArticle(), article);
		return article;
	}

	@Override
	public Article update(Integer idarticle) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Integer idarticle) {
		listArticle = ma.getMockArticle();
		listArticle.remove(idarticle);
		
	}

	
}
