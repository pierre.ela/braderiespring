/**
 * 
 */
package org.afpa.braderieSpring.dao.ImplArticle;

import java.util.HashMap;
import java.util.Map;

import org.afpa.braderieSpring.bean.Article;
import org.springframework.stereotype.Component;

/**
 * @author alexo
 * Mock d'articles
 * Classe bouchons pour simuler la table t_article
 * On créé des articles dans le constructeur pour que la
 * Map soit rempli à l'appel de la classe
 */

@Component
public class MockArticle {

	
	private Map<Integer, Article> mockArticle = new HashMap<Integer, Article>();
	
	public MockArticle() {		
		mockArticle.put(1, new Article(8, "Volet Roulant", "Velux", 355));
		mockArticle.put(2, new Article(9, "Carte électronique", "POA1/A", 159));
		mockArticle.put(3, new Article(10, "Alarme de fenêtre", "MM-52340", 16));
		mockArticle.put(4, new Article(11, "Kit Motorisation de portail", "Freevia", 1301));
	}

	public Map<Integer, Article> getMockArticle() {
		return mockArticle;
	}

	public void setMockArticle(Map<Integer, Article> mockArticle) {
		this.mockArticle = mockArticle;
	}
}
