package org.afpa.braderieSpring.dao;

import java.util.Optional;

import org.afpa.braderieSpring.bean.User;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Samia
 *
 */
@Repository
public interface DaoUser extends CrudRepository<User, Integer>{

	//Se présente comme un petit conteneur d'objets de type user
	public Optional<User> getByIduser (int idUser);
	
	
	@Procedure (value = "IsValidlogon")
	Boolean IsValidlogon(Boolean _val, String LG, String PW);
}
