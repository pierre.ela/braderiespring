package org.afpa.braderieSpring.ArticleRepository;


import org.afpa.braderieSpring.bean.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface ArticleRepository extends CrudRepository<Article, Integer> {
	
}
