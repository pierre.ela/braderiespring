/**
 * 
 */
package org.afpa.braderieSpring.service;
import java.util.Optional;

import org.afpa.braderieSpring.bean.User;
import org.afpa.braderieSpring.dao.DaoUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author samia
 *
 */


@Service (value="serviceUser")
public class ServiceUser {
	@Autowired
	private DaoUser daoU ;
	public ServiceUser() {
	
	}
	public Optional<User> getByIdser (int idUser){
		return daoU.getByIduser(idUser);
	}

}
