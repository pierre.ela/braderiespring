package org.afpa.braderieSpring.service;

import java.util.Optional;

import org.afpa.braderieSpring.ArticleRepository.ArticleRepository;
import org.afpa.braderieSpring.bean.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceArticle {

	
	@Autowired
	private ArticleRepository articleRepository;
	
	//OBTENIR LA LISTE DES ARTICLES
	public Iterable<Article> serviceListeArticle() {
		System.out.println("Dans ServiceArticle.serviceListeArticle");
		Iterable<Article> liste= articleRepository.findAll();
		liste.forEach(article -> System.out.println(article.getIdArticle()+" "+article.getDescription()));
		System.out.println("Il y a " + articleRepository.count()+" articles");
		return liste;
	}
	
	
	//OBTENIR UN ARTICLE PAR L'ID
	public Optional<Article> serviceGetArticle(Integer idarticle) {
		return articleRepository.findById(idarticle);	
	}

	
	//AJOUTER UN ARTICLE
	public Article addArticle(Article article) {
		System.out.println("Dans ArticleService.addArticle");
		articleRepository.save(article);		
		return article;
	}
	
	
	//SUPPRIMER UN ARTICLE PAR SON ID
	public void deleteArticleById(Integer idarticle) {
		System.out.println("Dans ArticleService.deleteArticleById");
		articleRepository.deleteById(idarticle);
	}
	
	
	public ArticleRepository getArticleRepository() {
		return articleRepository;
	}

	
	public void setArticleRepository(ArticleRepository articleRepository) {
		this.articleRepository = articleRepository;
	}
}
