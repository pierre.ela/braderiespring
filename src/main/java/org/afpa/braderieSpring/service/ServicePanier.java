package org.afpa.braderieSpring.service;

import java.util.Optional;

import org.afpa.braderieSpring.bean.Panier;
import org.afpa.braderieSpring.dao.DaoPanier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service (value="servicePanier")
public class ServicePanier {
	@Autowired
	private DaoPanier daoP ;
	public ServicePanier() {
		
	}
	/**
	 * @param idUser
	 * @return
	 */
	public Iterable<Panier> getByIdser (int idUser){
		return daoP.getByIduser(idUser);
	}
	
	/**
	 * @param panier
	 */
	public void delete (Panier panier) {
		daoP.delete(panier);
	}
	
	/**
	 * @param idPanier
	 * @return <Optional>Panier
	 */
	public Optional<Panier> getById(Integer idPanier) {
		return daoP.findById(idPanier);
	}
	
	/**
	 * @param idPanier
	 * @param Qte
	 */
	public void updateQte (Integer idPanier, Integer Qte) {
		Panier monPanier=this.getById(idPanier).get();
		monPanier.setQuantite(Qte);
		daoP.save(monPanier);
	}
	
	/**
	 * @param panier
	 */
	public void savePanier(Panier panier) {
		daoP.save(panier);
	}

}
